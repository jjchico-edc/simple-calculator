// File:        contro_unit.v
// Project:     simple calculator
// Description: Calculator's control unit
// Author:      Jorge Juan-Chico
// Date:        2021-03-19 (initial)

module control_unit (
    input wire clk,         // global clock
    input wire reset,       // reset (active-low)
    input wire start,       // start
    input wire [1:0] op,    // operation code

    output reg ready,       // ready for next operation
    output reg [1:0] sel,   // ALU code
    output reg rin,         // data input read control
    output reg wt,          // T register write control
    output reg ra,          // A register read control
    output reg wa,          // A register write control
    output reg rb,          // B register read control
    output reg wb           // B register write control
);

    // State definition
    localparam  READY   = 0,
                ADDSUB2 = 1,
                SWAP2   = 2,
                SWAP3   = 3;

    // State variables
    reg [1:0] state, next_state;

    // State change process
    always @(posedge clk)
        if (reset == 1'b0)
            state <= READY;
        else
            state <= next_state;

    // Next state and output calculation
    always @* begin
        // default values
        next_state = 'bx;
        ready = 1'b0; sel = 'b0; rin = 1'b0; wt = 1'b0;
        ra = 1'b0; wa = 1'b0; rb = 1'b0; wb = 1'b0;
        // next state and output assignment
        case (state)
        READY: begin
            ready = 1'b1;
            if (start == 1'b0)
                next_state = READY;
            else if (op == 2'b00 || op == 2'b01) begin
                ra = 1'b1; wt = 1'b1;
                next_state = ADDSUB2;
            end else if (op == 2'b10) begin
                rin = 1'b1; wa = 1'b1; sel = 2'b11;
                next_state = READY;
            end else begin          // op = 11
                rb = 1'b1; wt = 1'b1;
                next_state = SWAP2;
            end
        end
        ADDSUB2: begin
            rb = 1'b1; wa = 1'b1;
            sel[0] = op[0];         // take care if you change op or sel
            next_state = READY;
        end
        SWAP2: begin
            ra = 1'b1; wb = 1'b1; sel = 2'b11;
            next_state = SWAP3;
        end
        SWAP3: begin
            wa = 1'b1; sel = 2'b10;
            next_state = READY;
        end
        endcase
    end
endmodule