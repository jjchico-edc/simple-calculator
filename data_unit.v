// File:        data_unit.v
// Project:     simple calculator
// Description: Calculator's data unit
// Author:      Jorge Juan-Chico
// Date:        2021-03-19 (initial)

module data_unit #(
    parameter W = 8
)(
    input wire clk,         // clock signal
    input wire reset,       // global reset
    input wire [1:0] sel,   // ALU operation selector
    input wire rin,         // input reading control
    input wire [W-1:0] din, // input data
    input wire wt,          // T register write control
    input wire ra,          // A register read control
    input wire wa,          // A register write control
    input wire rb,          // B register read control
    input wire wb,          // B register write control

    output reg [W-1:0] rega,    // Register A
    output reg [W-1:0] regb     // Register B
);

    // Internal signals
    reg [W-1:0] alu_out;    // ALU output
    reg [W-1:0] regt;       // T register
    reg [W-1:0] bus;        // output bus

    // ALU
    always @*
        case(sel)
        2'b00:  alu_out = regt + bus;
        2'b01:  alu_out = regt - bus;
        2'b10:  alu_out = regt;
        2'b11:  alu_out = bus;
        endcase

    // T register
    always @(posedge clk)
        if (reset == 1'b0)
            regt <= 'b0;
        else if (wt)
            regt <= bus;

    // A register
    always @(posedge clk)
        if (reset == 1'b0)
            rega <= 'b0;
        else if (wa)
            rega <= alu_out;

    // B register
    always @(posedge clk)
        if (reset == 1'b0)
            regb <= 'b0;
        else if (wb)
            regb <= alu_out;

    // Output bus multiplexer
    always @*
        if (rin)
            bus = din;
        else if (ra)
            bus = rega;
        else if (rb)
            bus = regb;
        else
            bus = 'b0;
endmodule