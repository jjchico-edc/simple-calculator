// File:        calculator.v
// Project:     simple calculator
// Description: Calculator system. Data and control unit connection.
// Author:      Jorge Juan-Chico
// Date:        2021-03-19 (initial)

module calculator #(
    parameter W = 8
)(
    input wire clk,             // global clock
    input wire reset,           // reset (active-low)
    input wire start,           // start
    input wire [W-1:0] din,     // input data
    input wire [1:0] op,        // operation code

    output wire ready,          // ready for next operation
    output wire [W-1:0] rega,   // Register A
    output wire [W-1:0] regb    // Register B
);

    // Internal signals
    wire [1:0] sel;
    wire rin, wt, ra, wa, rb, wb;

    // Control unit instance
    control_unit control_unit (
        .clk(clk),          // global clock
        .reset(reset),      // reset (active-low)
        .start(start),      // start
        .op(op),            // operation code
        .ready(ready),      // ready for next operation
        .sel(sel),          // ALU code
        .rin(rin),          // data input read control
        .wt(wt),            // T register write control
        .ra(ra),            // A register read control
        .wa(wa),            // A register write control
        .rb(rb),            // B register read control
        .wb(wb)             // B register write control
    );

    // Data unit
    data_unit #(.W(W)) data_unit (
        .clk(clk),          // clock signal
        .reset(reset),      // global reset
        .sel(sel),          // ALU operation selector
        .rin(rin),          // input reading control
        .din(din),          // input data
        .wt(wt),            // T register write control
        .ra(ra),            // A register read control
        .wa(wa),            // A register write control
        .rb(rb),            // B register read control
        .wb(wb),            // B register write control

        .rega(rega),        // Register A
        .regb(regb)         // Register B
    );
endmodule
