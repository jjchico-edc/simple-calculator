// File:        system.v
// Project:     simple calculator
// Description: Calculator system for development board implementation.
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2023-02-12 (initial version)

module system #(
    parameter W = 4
)(
    input wire clk,             // clock (rising edge)
    input wire reset,           // reset button (synchronous, active-high)
    input wire start,           // start button
    input wire [W-1:0] din,     // data input
    input wire [1:0] op,        // operation input
    output wire ready,          // ready indicator
    output wire [3:0] an,       // 7-segment anode control
    output wire [0:6] seg,      // 7-segment code (active-low)
    output wire dp              // 7-segment decimal point output
);

    // Internal signals
    wire [W-1:0] rega, regb;
    reg start0, start1;

    // Edge detector
    always @(posedge(clk)) begin
        start0 <= start;
        if (start == 1'b1 && start0 == 1'b0)
            start1 <= 1'b1;
        else
            start1 <= 1'b0;
    end

    // Calculator instance
    calculator #(.W(W)) calculator (
        .clk(clk),          // global clock
        .reset(~reset),     // reset (active-low)
        .start(start1),     // start
        .din(din),          // input data
        .op(op),            // operation code
        .ready(ready),      // ready for next operation
        .rega(rega),        // Register A
        .regb(regb)         // Register B
    );

    // 7-segment controller instance
    display_ctrl #(.cdbits(18), .hex(1)) display_ctrl (
        .ck(clk),           // system clock
        .x3(4'h0),          // display digits, from left to right
        .x2(rega),
        .x1(4'h0),
        .x0(regb),
        .dp_in(4'b1011),    // decimal point vector
        .seg(seg),          // 7-segment output
        .an(an),            // anode output
        .dp(dp)             // decimal point output
    );
endmodule
