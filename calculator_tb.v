// File:        calculator_tb.v
// Project:     simple calculator
// Description: System's test bench
// Author:      Jorge Juan-Chico
// Date:        2021-03-19 (initial)

`timescale 1ns / 1ps

module test ();

    reg clk;            // global clock
    reg reset;          // reset (active-low)
    reg start;          // start
    reg [3:0] din;      // input data
    reg [1:0] op;       // operation code

    wire ready;         // ready for next operation
    wire [3:0] rega;    // Register A
    wire [3:0] regb;    // Register B

    calculator #(.W(4)) calculator (
        .clk(clk),      // global clock
        .reset(reset),  // reset (active-low)
        .start(start),  // start
        .din(din),      // input data
        .op(op),        // operation code
        .ready(ready),  // ready for next operation
        .rega(rega),    // Register A
        .regb(regb)     // Register B
    );

    // Clock generator (T=20ns, f=50MHz)
    always
        #10 clk = ~clk;

    // Main simulation process
    //      A <- 5
    //      SWAP(A,B)
    //      A <- 3
    //      A <- A + B      : A = 8, B = 5
    //      SWAP(A,B)
    //      A <- 12
    //      A <- A - B      : A = 4, B = 8
    initial begin
        // output generation
        $dumpfile("calculator_tb.vcd");
        $dumpvars(0, test);

        // input signal initialization
        clk = 1'b0;
        reset = 1'b1;
        start = 1'b0;
        din = 'b0;
        op = 'b0;

        // global reset
        @(posedge clk) #1 reset = 1'b0;
        @(posedge clk) #1 reset = 1'b1;

        repeat(3) @(posedge clk) #1;

        din = 5; op = 2;                // A <- 5
        start = 1'b1;
        @(posedge clk) #1;
        start = 1'b0;

        repeat(3) @(posedge clk) #1;

        // A should be 5 here

        op = 3;                         // SWAP(A,B)
        start = 1'b1;
        @(posedge clk) #1;
        start = 1'b0;

        repeat(3) @(posedge clk) #1;

        // B should be 5 here

        din = 3; op = 2;                // A <- 3
        start = 1'b1;
        @(posedge clk) #1;
        start = 1'b0;

        repeat(3) @(posedge clk) #1;

        op = 0;                         // A <- A + B
        start = 1'b1;
        @(posedge clk) #1;
        start = 1'b0;

        repeat(3) @(posedge clk) #1;

        op = 3;                         // SWAP(A,B)
        start = 1'b1;
        @(posedge clk) #1;
        start = 1'b0;

        repeat(3) @(posedge clk) #1;

        din = 12; op = 2;                // A <- 12
        start = 1'b1;
        @(posedge clk) #1;
        start = 1'b0;

        repeat(3) @(posedge clk) #1;

        op = 1;                         // A <- A - B
        start = 1'b1;
        @(posedge clk) #1;
        start = 1'b0;

        repeat(3) @(posedge clk) #1;

        $finish;
    end
endmodule
